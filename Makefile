CC=gcc
CFLAGS=-pthread -Wall -Wextra -Werror -Wpedantic -g

main: main.c
	$(CC) $(CFLAGS) -o main main.c

clean:
	rm -f main