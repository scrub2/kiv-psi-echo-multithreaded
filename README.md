# KIV-PSI ECHO multithreaded

This is a multithreaded implementation of the ECHO TCP server for the KIV/PSI course at the University of West Bohemia.

## Files

* `main.c` - source code containing the whole server
* `Makefile` - makefile for compiling the server with target `main` and `clean`
* `README.md` - this file

## Implementation

The TCP server is implemented in C language using the POSIX threads and BSD sockets.

The server listens on all available IP addresses with a specified port. After accepting a connection, a new thread is run that handles that specific connection: it reads from it and writes back whatever is recieved (prefixed with "You wrote:"). If the connection is closed by the other party, the thread closes the connection and returns. The main thread only initializes the server and accepts connections. Connection handling threads are detached so they don't create zombies in the thread/process table.

The `main.c` file contains the main function and the procedure for handling the clients. The `main` function runs in the main thread and parses the argument (PORT of the server) and starts the server. The "connection handle routine" is implemented by the `echo_handle` function.

### Discussion
A better architecture would be to have a pool of threads (workers) which handle multiple connections, that are assigned to them by the main thread, using e.g. the `select` function. This architecture was not implemented as not many connections are expected.

## Usage

To compile the server, run `make` in the directory with the `Makefile`. To start the server, run `./main PORT`, where `PORT` is the port number on which the server will listen for incoming connections. The server can be stopped by pressing `CTRL+C`. There is no client implementation for this server, but it can be tested using the `telnet` or `netcat` command.
