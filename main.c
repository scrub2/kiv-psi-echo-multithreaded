#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>

struct client_info {
    int fd;
    struct sockaddr_in addr;
};

void *echo_handle(void *arg)
{
    struct client_info *cinfo = (struct client_info *)arg;
    int fd = cinfo->fd;
    char buf[1024];
    int n;
    while (1) {
        n = read(fd, buf, sizeof(buf));
        if (n == 0) {
            printf("client %s:%d closed connection\n", inet_ntoa(cinfo->addr.sin_addr), ntohs(cinfo->addr.sin_port));
            break;
        }
        write(fd, "You wrote:", 10);
        write(fd, buf, n);
    }
    close(fd);
    free(cinfo);
    return NULL;
}

int main(int argc, char *argv[])
{
    if(argc != 2) {
        printf("Usage: %s <port>\n", argv[0]);
        exit(1);
    }

    int lfd, cfd;
    struct sockaddr_in serv_addr, client_addr;
    socklen_t client_addr_len;
    pthread_t tid;
    struct client_info *cinfo;

    lfd = socket(AF_INET, SOCK_STREAM, 0);
    if (lfd == -1) {
        perror("socket");
        exit(1);
    }

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port = htons(strtol(argv[1], NULL, 0));

    if (bind(lfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) == -1) {
        perror("bind");
        exit(1);
    }

    if (listen(lfd, 5) == -1) {
        perror("listen");
        exit(1);
    }

    while (1) {
        client_addr_len = sizeof(client_addr);
        cfd = accept(lfd, (struct sockaddr *)&client_addr, &client_addr_len);
        if (cfd == -1) {
            perror("accept");
            //exit(1);
            continue;
        }
        cinfo = malloc(sizeof(struct client_info));
        cinfo->fd = cfd;
        cinfo->addr = client_addr;
        printf("client %s:%d connected\n", inet_ntoa(client_addr.sin_addr), ntohs(client_addr.sin_port));
        pthread_create(&tid, NULL, echo_handle, (void *)cinfo);
	pthread_detach(tid); // avoid necromancy
    }

    return 0;
}
